New Media Design & Development I
================================

|Info|  |
|----|---|
|OLOD|New Media Design & Development I|
|Auteur(s)|Mathias Van Kerckvoorde, Dylan Van Steirteghem|
|Opleiding|Bachelor in de Grafische en Digitale Media|
|Academiejaar|2015-16|
|Opdracht|Werkstuk open_the_gates_for_data - MDG|

***

Dossier
=======

***

Table of Contents
-----------------
* [Briefing & Analyse](#briefing-analyse)
* [Functionele specificaties](#functionele-specificaties)
* [Technische specificaties](#technische-specificaties)
* [Persona's](#personas)
* [Moodboards](#moodboards)
* [Sitemap](#sitemap)
* [Wireframes](#wireframes)
* [Style Tiles](#style-tiles)
* [Visual Designs](#visual-designs)
* [Screenshots (final)](#screenshots-final)
* [Screenshots snippets](#screenshots-snippets)
* [Tijdbesteding per student](#tijdsbesteding-per-student)

***

##Briefing & Analyse

**Briefing**
Maak een responsive mobile-first webapplicatie waarin 6 datasets , afkomstig uit de dataset-pool van de Worldbank, verwerkt zijn. Conceptueel denken is heel belangrijk in deze applicatie. Dit betekent dat jullie verbanden moeten leggen tussen deze datasets. Het is louter niet alleen het oplijsten en visualiseren van deze datasets, er moet een concept rond gebouwd worden.

Deze applicatie is bestemd voor +18 jarigen waarbij de gebruiksvriendelijkheid, interactiviteit, uitbreidbaarheid en schaalbaarheid van belang zijn.

**Analyse**

WIP

##Functionele specificaties

* Webapplicatie bestaande uit verschillende pagina's
* Inladen van JSON(P)
* De meeste inhoud wordt beheerd in data bastanden en dynamisch ingeladen
* Adaptive images, video's en sounds
* YouTube integratie
* Custom look-and-feel
* Animaties via SVG (Chartist)
* Gebruiker ervaart een interactief webapplicatie
* Gebruiker kan de webapplicatie bookmarken in browser
* Automation
	* SASS bestanden worden automatisch omgezet in corresponderende CSS-bestanden
	* CSS-bestanden worden met elkaar verbonden in één bestand en geminified
	* De JS code wordt automatisch nagekeken op syntax fouten JS-bestanden worden met elkaar verbonden in één bestand en geminified
	* De dist-folder wordt automatisch aangevuld met bestanden en folders via Grunt
	* Screenshots van de verschillende breekpunten worden automatisch uitgevoerd via Phantom

##Technische specificaties

**Frontend**

- Core: HTML5, CSS3 en JavaScript
- Storage: JSON bestanden (van WorldBank)
- Bibliotheken: jQuery, lodash.js, chartist.js, grunt.js, bower.js, ...

##Persona's

![Persona1](images/persona's/persona_jan.png) ![Persona2](images/persona's/persona_elise.png) ![Persona3](images/persona's/persona_david.png)

##Moodboards

![MoodboardFinal](images/moodboards/moodboard_final.png) ![Moodboard1](images/moodboards/moodboard1.png) ![Moodboard2](images/moodboards/moodboard2.png)

##Sitemap
![Sitemap](images/sitemap/sitemap.png)

##Wireframes

**Desktop**
![WireframeDesktopHome](images/wireframes/desktop/wireframe_desktop_home.png) ![WireframeDesktopAbout](images/wireframes/desktop/wireframe_desktop_about.png) ![WireframeDesktopContact](images/wireframes/desktop/wireframe_desktop_contact.png)
![WireframeDesktopHomeHover](images/wireframes/desktop/wireframe_desktop_homehover.png) ![WireframeDesktopGoal](images/wireframes/desktop/wireframe_desktop_goal.png) ![WireframeDesktopInformation](images/wireframes/desktop/wireframe_desktop_information.png)
![WireframeDesktopStatistics](images/wireframes/desktop/wireframe_desktop_statistics.png) ![WireframeDesktopStatisticsDetail](images/wireframes/desktop/wireframe_desktop_statisticsdetail.png) ![WireframeDesktopRelatedVids](images/wireframes/desktop/wireframe_desktop_relatedvids.png)
![WireframeDesktopLegende](images/wireframes/desktop/wireframe_desktop_legende.png)

**Mobile**
![WireframeMobileHome](images/wireframes/mobile/wireframe_mobile_home.png) ![WireframeMobileAbout](images/wireframes/mobile/wireframe_mobile_about.png) ![WireframeMobileContact](images/wireframes/mobile/wireframe_mobile_contact.png)
![WireframeMobileHomeHover](images/wireframes/mobile/wireframe_mobile_homehover.png) ![WireframeMobileGoal](images/wireframes/mobile/wireframe_mobile_goal.png) ![WireframeMobileInformation](images/wireframes/mobile/wireframe_mobile_information.png)
![WireframeMobileStatistics](images/wireframes/mobile/wireframe_mobile_statistics.png) ![WireframeMobileStatisticsDetail](images/wireframes/mobile/wireframe_mobile_statisticsdetail.png) ![WireframeMobileRelatedVids](images/wireframes/mobile/wireframe_mobile_relatedvids.png)
![WireframeMobileLegende](images/wireframes/mobile/wireframe_mobile_legende.png)

##Style Tiles

![StyleTileFinal](images/style_tiles/style_tile_final.png) ![StyleTile1](images/style_tiles/style_tile1.png) ![StyleTile2](images/style_tiles/style_tile2.png)

##Visual Designs

**Desktop**
![VisualDesignDesktopHome](images/visual_designs/desktop/visualdesign_desktop_home.png) ![VisualDesignDesktopAbout](images/visual_designs/desktop/visualdesign_desktop_about.png) ![VisualDesignDesktopContact](images/visual_designs/desktop/visualdesign_desktop_contact.png)
![VisualDesignDesktopHomeHover](images/visual_designs/desktop/visualdesign_desktop_homehover.png) ![VisualDesignDesktopGoal](images/visual_designs/desktop/visualdesign_desktop_goal.png) ![VisualDesignDesktopInformation](images/visual_designs/desktop/visualdesign_desktop_information.png)
![VisualDesignDesktopStatistics](images/visual_designs/desktop/visualdesign_desktop_statistics.png) ![VisualDesignDesktopStatisticsDetail](images/visual_designs/desktop/visualdesign_desktop_statisticsdetail.png) ![VisualDesignDesktopRelatedVids](images/visual_designs/desktop/visualdesign_desktop_relatedvids.png)

**Mobile**
![VisualDesignMobileHome](images/visual_designs/mobile/visualdesign_mobile_home.png) ![VisualDesignMobileAbout](images/visual_designs/mobile/visualdesign_mobile_about.png) ![VisualDesignMobileContact](images/visual_designs/mobile/visualdesign_mobile_contact.png)
![VisualDesignMobileHomeHover](images/visual_designs/mobile/visualdesign_mobile_homehover.png) ![VisualDesignMobileGoal](images/visual_designs/mobile/visualdesign_mobile_goal.png) ![VisualDesignMobileInformation](images/visual_designs/mobile/visualdesign_mobile_information.png)
![VisualDesignMobileStatistics](images/visual_designs/mobile/visualdesign_mobile_statistics.png) ![VisualDesignMobileStatisticsDetail](images/visual_designs/mobile/visualdesign_mobile_statisticsdetail.png) ![VisualDesignMobileRelatedVids](images/visual_designs/mobile/visualdesign_mobile_relatedvids.png)

##Screenshots (final)

![ScreenshotHome](images/screenshots_final/screenshot_home.png) ![ScreenshotAbout](images/screenshots_final/screenshot_about.png) ![VisualDesignMobileContact](images/screenshots_final/screenshot_contact.png)
![ScreenshotMobileHomeHover](images/screenshots_final/screenshot_homehover.png) ![ScreenshotMobileGoal](images/screenshots_final/screenshot_goal.png) ![ScreenshotMobileInformation](images/screenshots_final/screenshot_information.png)
![ScreenshotMobileStatistics](images/screenshots_final/screenshot_statistics.png) ![ScreenshotMobileStatisticsDetail](images/screenshots_final/screenshot_statisticsdetail.png) ![ScreenshotMobileRelatedVids](images/screenshots_final/screenshot_relatedvids.png)

##Screenshots snippets



##Tijdsbesteding per student

* [Timesheet.xlsx](https://bitbucket.org/gdm15162mmpa11/open_the_gates_for_data_docs/src/c5b58d02c63c92866c7416ab2f58d568f83f15aa/timesheet.xlsx)

***

Auteurs
-------
**Mathias Van Kerckvoorde**  

* [GitHub Account](https://github.com/mathvank1994/)
* [Bitbucket Account](https://bitbucket.org/mathvank/)
* <mathias.vankerckvoorde@student.arteveldehs.be>

**Dylan Van Steirteghem**

* [GitHub Account](https://github.com/dylavans)
* [Bitbucket Account](https://bitbucket.org/dylavans/)
* <dylan.vansteirteghem@student.arteveldehs.be>
 
Arteveldehogeschool
-------------------

- <http://www.arteveldehogeschool.be>
- <http://www.arteveldehogeschool.be/ects>
- <http://www.arteveldehogeschool.be/bachelor-de-grafische-en-digitale-media>
- <http://twitter.com/bachelorGDM>
- <https://www.facebook.com/BachelorGrafischeEnDigitaleMedia?fref=ts>