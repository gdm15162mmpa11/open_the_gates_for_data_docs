New Media Design & Development I
================================

|Info|  |
|----|---|
|OLOD|New Media Design & Development I|
|Auteur(s)|Mathias Van Kerckvoorde, Dylan Van Steirteghem|
|Opleiding|Bachelor in de Grafische en Digitale Media|
|Academiejaar|2015-16|
|Opdracht|Werkstuk open_the_gates_for_data - MDG|

***

Documenten
----------
* [Dossier](https://bitbucket.org/gdm15162mmpa11/open_the_gates_for_data_docs/src/a6e89c54a6bf94f6e075e726cf1bc98bcd8c0a2d/dossier.md)

**Team Member: Mathias**
* [Github - open_the_gates_for_data_app](https://github.com/mathvank1994/open_the_gates_for_data_app)
* [Github - open_the_gates_for_data_docs](https://github.com/mathvank1994/open_the_gates_for_data_docs)

**Team Member: Dylan**
* [Github - open_the_gates_for_data_app](https://github.com/dylavans/open_the_gates_for_data_app)
* [Github - open_the_gates_for_data_docs](https://github.com/dylavans/open_the_gates_for_data_docs)

**Team**
* [Bitbucket - open_the_gates_for_data_app](https://bitbucket.org/gdm15162mmpa11/open_the_gates_for_data_app)
* [Bitbucket - open_the_gates_for_data_docs](https://bitbucket.org/gdm15162mmpa11/open_the_gates_for_data_docs)

Synopsis webapplicatie
----------------------

Een mobile-first applicatie die de focus legt op de millenniumdoelstellingen, met duidelijke informatie over de momentele staat van de aangewezen landen en hun leefmilieu, in combinatie met de doeleinden van het millenniumdoelstellingen plan. 

De applicatie zal de data van de World Bank toegankelijk en leesbaar maken voor de modale mens, hiervoor wordt er gebruik gemaakt van een responsive design, uitgebreid aanbod aan officiële informatie, aangekleed met grafieken. Het doel is om de gebruiker te informeren over de toestand van de wereld op vlak van natuur en klimaat, en hoever we nu staan tegenover de MDG’s.

De millenniumdoelstellingen zijn vertaald in acht concrete doelen, die – tenzij anders vermeld - in 2015 moeten worden behaald. Per doel zijn prestatie-indicatoren vastgesteld, die apart te meten zijn. De voortgang wordt gemeten ten opzichte van de situatie in 1990.

Webapplicatie
-------------

* Link to AHS Hosting (mathvank) - Webapplication: www.arteveldehogeschool.be/campusGDM/studenten_201516/mathvank/open_the_gates_for_data_app/app/index.html
* Link to AHS Hosting (dylavans) - Webapplication:(www.arteveldehogeschool.be/campusGDM/studenten_201516/dylavans/open_the_gates_for_data_app/app/index.html)

Auteurs
------- 
**Mathias Van Kerckvoorde**  

* [GitHub Account](https://github.com/mathvank1994/)
* [Bitbucket Account](https://bitbucket.org/mathvank/)
* <mathias.vankerckvoorde@student.arteveldehs.be>

**Dylan Van Steirteghem**

* [GitHub Account](https://github.com/dylavans/)
* [Bitbucket Account](https://bitbucket.org/dylavans/)
* <dylan.vansteirteghem@student.arteveldehs.be>
 
Arteveldehogeschool
-------------------

- <http://www.arteveldehogeschool.be>
- <http://www.arteveldehogeschool.be/ects>
- <http://www.arteveldehogeschool.be/bachelor-de-grafische-en-digitale-media>
- <http://twitter.com/bachelorGDM>
- <https://www.facebook.com/BachelorGrafischeEnDigitaleMedia?fref=ts>